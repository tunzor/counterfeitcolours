package com.tunzor.counterfeit_colours;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by arusso on 1/27/2017.
 */

public class PlayActivity extends FragmentActivity {

    TextView gridNW, gridN, gridNE, gridW, gridC, gridE, gridSW, gridS, gridSE, tapColour, feedback, points, timer;
    RelativeLayout scoreArea;
    android.support.v7.widget.GridLayout grid;

    String currentColour = "";
    int currentScore = 0;
    int secondsRemaining = 0;
    int gameMode = 0;
    FragmentManager fm = getSupportFragmentManager();
    CountDownTimer countdownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//hides title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//hides notification bar
        setContentView(R.layout.play_activity);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            secondsRemaining = b.getInt("gameTime");
            gameMode = secondsRemaining;
        }

        gridNW = (TextView) findViewById(R.id.play_activity_nw);
        gridN = (TextView) findViewById(R.id.play_activity_n);
        gridNE = (TextView) findViewById(R.id.play_activity_ne);
        gridW = (TextView) findViewById(R.id.play_activity_w);
        gridC = (TextView) findViewById(R.id.play_activity_c);
        gridE = (TextView) findViewById(R.id.play_activity_e);
        gridSW = (TextView) findViewById(R.id.play_activity_sw);
        gridS = (TextView) findViewById(R.id.play_activity_s);
        gridSE = (TextView) findViewById(R.id.play_activity_se);

        feedback = (TextView) findViewById(R.id.play_activity_feedback_text);
        tapColour = (TextView) findViewById(R.id.play_activity_tap_colour);

        points = (TextView) findViewById(R.id.play_activity_points);
        points.setText(getScore());
        timer = (TextView) findViewById(R.id.play_activity_timer);
        timer.setText(getTimeRemaining());
        setGridViewColours(ColoursUtil.getShuffledColours());

        grid = (android.support.v7.widget.GridLayout) findViewById(R.id.play_activity_grid);
        scoreArea = (RelativeLayout) findViewById(R.id.play_score_container);

        countdownTimer = new CountDownTimer(secondsRemaining*1000, 1000) {
            public void onTick(long millisUntilFinished) {
                secondsRemaining--;
                timer.setText(getTimeRemaining());
            }
            public void onFinish() {
                timer.setText(getString(R.string.timer_zero_value));
                timer.setBackgroundColor(Color.parseColor("red"));
                unregisterClickableGrid();
                GameOverDialogFragment dialog = new GameOverDialogFragment();
                if(!fm.isDestroyed()) {
                    dialog.show(fm, "");
                }
            }
        }.start();
    }

    private void setGridViewColours(ArrayList<Colour> colours) {
        Colour randColour = ColoursUtil.getRandomColour();
        currentColour = randColour.getColourName();
        tapColour.setBackgroundColor(Color.parseColor(randColour.getColourValue()));

        gridNW.setText(colours.get(0).getColourName());
        gridNW.setTextColor(Color.parseColor(colours.get(0).getColourValue()));

        gridN.setText(colours.get(1).getColourName());
        gridN.setTextColor(Color.parseColor(colours.get(1).getColourValue()));

        gridNE.setText(colours.get(2).getColourName());
        gridNE.setTextColor(Color.parseColor(colours.get(2).getColourValue()));

        gridW.setText(colours.get(3).getColourName());
        gridW.setTextColor(Color.parseColor(colours.get(3).getColourValue()));

        gridC.setText(colours.get(4).getColourName());
        gridC.setTextColor(Color.parseColor(colours.get(4).getColourValue()));

        gridE.setText(colours.get(5).getColourName());
        gridE.setTextColor(Color.parseColor(colours.get(5).getColourValue()));

        gridSW.setText(colours.get(6).getColourName());
        gridSW.setTextColor(Color.parseColor(colours.get(6).getColourValue()));

        gridS.setText(colours.get(7).getColourName());
        gridS.setTextColor(Color.parseColor(colours.get(7).getColourValue()));

        gridSE.setText(colours.get(8).getColourName());
        gridSE.setTextColor(Color.parseColor(colours.get(8).getColourValue()));
    }

    public void checkAnswer(View v) {
        TextView tv = (TextView) v;
        if(tv.getText().toString().equalsIgnoreCase(currentColour)) {
            feedback.setText(getString(R.string.play_correct));
            //feedback.setBackgroundColor(Color.parseColor("#00FF00"));
            scoreArea.setBackgroundColor(Color.parseColor("#00FF00"));
            currentScore++;
            points.setText(getScore());
            new CountDownTimer(500, 10) {
                public void onTick(long millisUntilFinished) {
                }
                public void onFinish() {
                    feedback.setText("");
                    //feedback.setBackgroundColor(Color.parseColor("#52d6ed"));
                    scoreArea.setBackgroundColor(Color.parseColor("#52d6ed"));
                }
            }.start();
        } else {
            feedback.setText(getString(R.string.play_incorrect));
            //feedback.setBackgroundColor(Color.parseColor("#FF0000"));
            scoreArea.setBackgroundColor(Color.parseColor("#FF0000"));
            if(currentScore > 0) {
                currentScore--;
                points.setText(getScore());
            }
            new CountDownTimer(500, 10) {
                public void onTick(long millisUntilFinished) {
                }
                public void onFinish() {
                    feedback.setText("");
                    //feedback.setBackgroundColor(Color.parseColor("#52d6ed"));
                    scoreArea.setBackgroundColor(Color.parseColor("#52d6ed"));

                }
            }.start();
        }
        setGridViewColours(ColoursUtil.getShuffledColours());
    }

    private String getScore() {
        return currentScore + " pts";
    }

    private String getTimeRemaining() {
        if(secondsRemaining < 10) {
            return "00:0"+ secondsRemaining;
        } else {
            return "00:" + secondsRemaining;
        }
    }

    private void unregisterClickableGrid() {
        gridNW.setClickable(false);
        gridN.setClickable(false);
        gridNE.setClickable(false);
        gridW.setClickable(false);
        gridC.setClickable(false);
        gridE.setClickable(false);
        gridSW.setClickable(false);
        gridS.setClickable(false);
        gridSE.setClickable(false);
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public int getGameMode() {
        return gameMode;
    }
}
