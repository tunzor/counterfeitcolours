package com.tunzor.counterfeit_colours;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by arusso on 2/8/2017.
 */

public class ScoresAdapter extends BaseAdapter {
    private Context thisContext;
    private ArrayList<Score> scoresArray;
    private boolean allMode;

    public ScoresAdapter (Context context, ArrayList<Score> scores) {
        this.thisContext = context;
        this.scoresArray = scores;
        this.allMode = false;
    }
    public ScoresAdapter (Context context, ArrayList<Score> scores, boolean allMode) {
        this.thisContext = context;
        this.scoresArray = scores;
        this.allMode = allMode;
    }
    @Override
    public int getCount() {
        if(scoresArray.size() == 0) {
            return 0;
        } else {
            return scoresArray.size();
        }
    }

    @Override
    public Score getItem(int position) {
        if(position >= 0 && position < scoresArray.size()) {
            return scoresArray.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Score score = getItem(position);
        View view;

        if(convertView == null) {
            view = LayoutInflater.from(thisContext).inflate(R.layout.high_scores_items, parent, false);
        } else {
            view = convertView;
        }

        TextView tv_player_name = (TextView) view.findViewById(R.id.high_score_list_name);
        tv_player_name.setText(score.getPlayerName());
        if(allMode) {
            tv_player_name.append(" - "+Integer.toString(score.getGameMode())+" sec");
        }

        TextView tv_score = (TextView) view.findViewById(R.id.high_score_list_score);
        tv_score.setText(Integer.toString(score.getPlayerScore()));

        return view;
    }
}
