package com.tunzor.counterfeit_colours;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Anthony on 2/7/2017.
 */

public class ClearScoresDialogFragment extends DialogFragment {

    View view;
    HighScoresActivity play;
    DBHelper db;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(getActivity() != null) {
            db = new DBHelper(getContext());
        }
        play = (HighScoresActivity) getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.clear_scores_message_dialog);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(clearScores()) {
                    play.showHighScores(0);
                    Toast.makeText(play, "Scores cleared.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(play, "Error. Scores not cleared.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        setCancelable(false);
        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.clear_scores_dialog, container, false);
        getDialog().setTitle("Clear scores");
        return view;
    }

    private boolean clearScores() {
        return db.clearScores();
    }
}
