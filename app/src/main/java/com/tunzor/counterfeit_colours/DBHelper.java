package com.tunzor.counterfeit_colours;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by arusso on 2/8/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context) {
        super(context,"CC_DB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE scores"+
                "(_id INTEGER primary key, playerName TEXT, playerScore INTEGER, gameModeLength INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF EXISTS TABLE scores");
        onCreate(db);
    }

    public boolean insertScore(Score score) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("playerName", score.getPlayerName());
        values.put("playerScore", score.getPlayerScore());
        values.put("gameModeLength", score.getGameMode());

        return db.insert("scores", null, values) != -1;
    }

    public ArrayList<Score> getScores(int mode) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList list = new ArrayList();
        Cursor res;
        if(mode == 0) { //0 means all scores
            res = db.rawQuery("SELECT * FROM scores ORDER BY playerScore DESC", null);
        } else {
            res = db.rawQuery("SELECT * FROM scores WHERE gameModeLength="+mode+" ORDER BY playerScore DESC", null);
        }
        if(res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                Score tempScore = new Score();
                tempScore.setScoreId(res.getInt(res.getColumnIndex("_id")));
                tempScore.setPlayerName(res.getString(res.getColumnIndex("playerName")));
                tempScore.setPlayerScore(res.getInt(res.getColumnIndex("playerScore")));
                tempScore.setGameMode(res.getInt(res.getColumnIndex("gameModeLength")));
                list.add(tempScore);
                res.moveToNext();
            }
        }
        res.close();
        return list;
    }

    public boolean clearScores() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("scores", null, null) > 0;
    }
}
