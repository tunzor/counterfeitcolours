package com.tunzor.counterfeit_colours;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

public class HighScoresActivity extends FragmentActivity {

    ListView highScoreListView;
    TextView clearButton, populateScores;
    DBHelper db = new DBHelper(this);
    FragmentManager fm = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//hides title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//hides notification bar
        setContentView(R.layout.high_scores_activity);

        highScoreListView = (ListView) findViewById(R.id.high_score_list);
        clearButton = (TextView) findViewById(R.id.high_score_instructions_title);
        populateScores = (TextView) findViewById(R.id.high_score_instructions);
        showHighScores(0);

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearScoresDialogFragment dialog = new ClearScoresDialogFragment();
                if(!fm.isDestroyed()) {
                    dialog.show(fm, "");
                }
            }
        });
    }

    public void gameTimeClicked(View v) {
        RadioButton rb = (RadioButton) findViewById(v.getId());
        if(rb.getId() == findViewById(R.id.high_score_all_time).getId()) {
            showHighScores(0);
        } else if(rb.getId() == findViewById(R.id.high_score_15seconds_time).getId()) {
            showHighScores(15);
        } else if(rb.getId() == findViewById(R.id.high_score_30seconds_time).getId()) {
            showHighScores(30);
        } else if(rb.getId() == findViewById(R.id.high_score_60seconds_time).getId()) {
            showHighScores(60);
        }
    }

    public void showHighScores(int mode) {
        ListView lv;
        ArrayList scoresList = db.getScores(mode);
        if(scoresList.isEmpty()) {
            lv = (ListView)findViewById(highScoreListView.getId());
            TextView emptyText = (TextView) findViewById(R.id.high_score_empty_list);
            lv.setEmptyView(emptyText);
        }
        ScoresAdapter adapter;
        if(mode == 0) {
            adapter = new ScoresAdapter(getApplicationContext(), scoresList, true);
        } else {
            adapter = new ScoresAdapter(getApplicationContext(), scoresList);
        }
        lv = (ListView)findViewById(highScoreListView.getId());
        lv.setAdapter(adapter);
    }
}
