package com.tunzor.counterfeit_colours;

/**
 * Created by arusso on 2/8/2017.
 */

public class Score {

    int scoreId;
    String playerName;
    int playerScore;
    int gameMode;

    public Score() {
        playerName = "";
        playerScore = 0;
        gameMode = 15;
    }

    public Score(String name, int score, int mode) {
        this.playerName = name;
        this.playerScore = score;
        this.gameMode = mode;
    }

    public int getScoreId() {
        return scoreId;
    }

    public Score setScoreId(int id) {
        this.scoreId = id;
        return this;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Score setPlayerName(String playerName) {
        this.playerName = playerName;
        return this;
    }

    public int getPlayerScore() {
        return playerScore;
    }

    public Score setPlayerScore(int playerScore) {
        this.playerScore = playerScore;
        return this;
    }

    public int getGameMode() {
        return gameMode;
    }

    public Score setGameMode(int gameMode) {
        this.gameMode = gameMode;
        return this;
    }
}
