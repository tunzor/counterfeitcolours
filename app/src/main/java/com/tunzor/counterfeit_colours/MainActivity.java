package com.tunzor.counterfeit_colours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends Activity {

    TextView playButton, highScoreButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//hides title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//hides notification bar
        setContentView(R.layout.activity_main);
        playButton = (TextView) findViewById(R.id.main_act_play_button);
        highScoreButton = (TextView) findViewById(R.id.main_act_high_score_button);

        ArrayList<Colour> colours = ColoursUtil.getShuffledColours();

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, SetupPlayActivity.class);
                startActivity(in);
                //finish();
            }
        });

        highScoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, HighScoresActivity.class);
                startActivity(in);
                //finish();
            }
        });
    }
}
