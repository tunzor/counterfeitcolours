package com.tunzor.counterfeit_colours;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by arusso on 1/26/2017.
 */

public final class ColoursUtil {

    static String coloursArray[] = {"blue","red","green","yellow","purple","orange","black","brown","grey"};
    static String coloursArrayHex[] = {"#0000FF","#FF0000","#2FB127","#FFEE00","#9122BC","#E86700","#000000","#91531C","#999999"};
    static ArrayList<String> colours = new ArrayList<String>(Arrays.asList(coloursArray));
    static ArrayList<String> coloursHex = new ArrayList<String>(Arrays.asList(coloursArrayHex));

    private ColoursUtil() {
    }

    public static ArrayList<Colour> getShuffledColours() {
        ArrayList<Colour> colourList = new ArrayList();
        ArrayList<String> tempColours1 = new ArrayList<>(colours);
        Collections.shuffle(tempColours1);
        ArrayList<String> tempColours2 = new ArrayList<>(tempColours1);

        String tempElement = tempColours2.get(0);
        tempColours2.remove(0);
        tempColours2.add(tempElement);
        tempColours2 = toHex(tempColours2);

        while(!tempColours1.isEmpty() && !tempColours2.isEmpty()) {
            colourList.add(new Colour(tempColours1.get(0),tempColours2.get(0)));
            tempColours1.remove(0);
            tempColours2.remove(0);
        }
        return colourList;
    }

    private static ArrayList<String> toHex(ArrayList<String> colours) {
        ArrayList<String> list = new ArrayList<>();
        for(int i = 0; i < colours.size(); i++) {
            list.add(toHex(colours.get(i)));
        }
        return list;
    }

    public static String toHex(String colour) {
        String hex = "";
        if (colour.equalsIgnoreCase("blue")) {
            hex = coloursHex.get(0);
        } else if (colour.equalsIgnoreCase("red")) {
            hex = coloursHex.get(1);
        } else if (colour.equalsIgnoreCase("green")) {
            hex = coloursHex.get(2);
        } else if (colour.equalsIgnoreCase("yellow")) {
            hex = coloursHex.get(3);
        } else if (colour.equalsIgnoreCase("purple")) {
            hex = coloursHex.get(4);
        } else if (colour.equalsIgnoreCase("orange")) {
            hex = coloursHex.get(5);
        } else if (colour.equalsIgnoreCase("black")) {
            hex = coloursHex.get(6);
        } else if (colour.equalsIgnoreCase("brown")) {
            hex = coloursHex.get(7);
        } else if (colour.equalsIgnoreCase("grey")) {
            hex = coloursHex.get(8);
        }
        return hex;
    }

    public static Colour getRandomColour() {
        int rand = (int)(Math.random() * 8);
        return new Colour(coloursArray[rand], coloursArrayHex[rand]);
    }
}
