package com.tunzor.counterfeit_colours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class SetupPlayActivity extends Activity {

    Button playButton;
    int gameTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//hides title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//hides notification bar
        setContentView(R.layout.setup_play_activity);

        playButton = (Button) findViewById(R.id.setup_play_button);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gameTime == 0) {
                    Toast.makeText(getApplicationContext(), "Please choose a game length.", Toast.LENGTH_SHORT).show();
                } else {
                    Intent in = new Intent(SetupPlayActivity.this, PlayActivity.class);
                    in.putExtra("gameTime", gameTime);
                    startActivity(in);
                    finish();
                }
            }
        });
    }

    public void gameTimeClicked(View v) {
        RadioButton rb = (RadioButton) findViewById(v.getId());
        if(rb.getId() == findViewById(R.id.setup_15seconds_time).getId()) {
            gameTime = 15;
        } else if(rb.getId() == findViewById(R.id.setup_30seconds_time).getId()) {
            gameTime = 30;
        } else if(rb.getId() == findViewById(R.id.setup_60seconds_time).getId()) {
            gameTime = 60;
        }
    }
}
