package com.tunzor.counterfeit_colours;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by Anthony on 2/7/2017.
 */

public class GameOverDialogFragment extends DialogFragment {

    View view;
    EditText name;
    PlayActivity play;
    DBHelper db;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(getActivity() != null) {
            name = new EditText(getActivity());
            name.setFilters(new InputFilter[] {new InputFilter.LengthFilter(20)});
            name.setInputType(InputType.TYPE_CLASS_TEXT);
            db = new DBHelper(getContext());
        }
        play = (PlayActivity) getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.game_over_message_dialog);
        builder.setView(name);
        builder.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(name.getText().length() != 0) {
                    Log.d("GameOverFragment", "player name: "+name.getText()+" "+play.getCurrentScore());
                    addScore(name.getText().toString(), play.getCurrentScore(), play.getGameMode());
                }
                Intent in = new Intent(getContext(), SetupPlayActivity.class);
                startActivity(in);
                dismissAllowingStateLoss();
                getActivity().finish();
            }
        });
        builder.setNegativeButton("QUIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(name.getText().length() != 0) {
                    Log.d("GameOverFragment", "player name: "+name.getText()+" "+play.getCurrentScore());
                    addScore(name.getText().toString(), play.getCurrentScore(), play.getGameMode());
                }
                dismissAllowingStateLoss();
                getActivity().finish();
            }
        });
        setCancelable(false);
        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.game_over_dialog, container, false);
        //name = (EditText) view.findViewById(R.id.game_over_player_name);
        getDialog().setTitle("GAME OVER!");
        return view;
    }

    private boolean addScore(String name, int score, int mode) {
        return db.insertScore(new Score(name, score, mode));
    }
}
