package com.tunzor.counterfeit_colours;

/**
 * Created by arusso on 1/26/2017.
 */

public class Colour {

    private String colourName;
    private String colourValue;

    public Colour() {

    }

    public Colour(String name, String value) {
        this.colourName = name;
        this.colourValue = value;
    }

    public String getColourName() {
        return colourName;
    }

    public Colour setColourName(String colourName) {
        this.colourName = colourName;
        return this;
    }

    public String getColourValue() {
        return colourValue;
    }

    public Colour setColourValue(String colourValue) {
        this.colourValue = colourValue;
        return this;
    }
}
