# Counterfeit Colours #
A game that shows a grid of colour names and asks the user to tap on the *name* of the colour displayed **not** the colour of the word.

Example: The colour of the box given is orange so the player must tap on the word ORANGE instead of the word that has a text colour of orange.

## Learning Experiences ##
In developing this project, I learned about and used these:

* Hiding the notification and title bars for fullscreen play with android.view.Window and WindowManager

* Having multiple view elements use the same onClick method

* android.os.CountDownTimer 

* Using styles.xml to define a style for multiple common elements

* DialogFragments and how to ensure user taps on dialog options


##Screenshots##

###Main Screen###
![main.png](https://bitbucket.org/repo/zxk84R/images/1680978173-main.png)

###Game Screen###
![playScreen.png](https://bitbucket.org/repo/zxk84R/images/907328331-playScreen.png)

###High Score Screen###
![highScoreScreen.png](https://bitbucket.org/repo/zxk84R/images/2690391030-highScoreScreen.png)